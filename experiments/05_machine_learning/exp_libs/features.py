import numpy as np
import nltk
from scipy import spatial
from zss import simple_distance, Node
from sklearn.feature_extraction.text import CountVectorizer
import pandas as pd

# ===============================
# PREPROCESSING FUNCTIONS
# ===============================
def create_preprocess_not(nlp):
    
    def preprocess_not(x):
        x  = nlp(x)
        tokens = []
        prefix = ''
        for t in x:
            if t.dep_ == 'neg':
                prefix = 'neg_'
                continue
            elif t.dep_ == 'punct':
                prefix = ''
                continue

            text = ''
            if t.pos_ == 'PRON':
                text = t.text.lower()
            else:
                text = t.lemma_
            fmt = '{}{}'.format(prefix, text)
            tokens.append(fmt)
        return tokens

    return preprocess_not
    
def create_preprocess_basic(nlp):
    
    def preprocess_basic_fn(x):
        tokens = []
        for t in nlp(x):
            if t.pos_ in ['PUNCT', 'DET'] or len(t.text.lower()) < 1:
                continue
            if t.pos_ in ['PRON', 'PROPN']:
                text = t.text.lower()
            else:
                text = t.lemma_
            tokens.append(text)
        return tokens
    
    return preprocess_basic_fn

def preprocess_stops(x):
    return [t.lemma_ for t in nlp_en(x) if not t.pos_ in ['PUNCT', 'DET'] and not t.lemma_ in pp.EN_STOPS]

# ===============================
# LEXICAL STRUCTURE FUNCTIONS
# ===============================

def cosine_coefficient(s1, s2):
    s1 = set(s1)
    s2 = set(s2)
    num = len(s1.intersection(s2))
    den = np.sqrt(len(s1)) * np.sqrt(len(s2))
    return num / (den + 1e-30)

def create_count_vectorizer_fn(tokenizer_fn, ngram_range, stopwords):
    
    count_vec = CountVectorizer(tokenizer=tokenizer_fn, ngram_range=ngram_range, stop_words=stopwords)
    
    def count_vectorizer_fn(ma, sa):
        try:
            x = count_vec.fit_transform([ma, sa])
        except ValueError:
            return 0.0
        
        mat = x.toarray()
        ma_vec = np.array(mat[0])
        sa_vec = np.array(mat[1])
        value = spatial.distance.cosine(ma_vec, sa_vec)
        if np.isnan(value):
            return 0.0
        else:
            return 1 - spatial.distance.cosine(ma_vec, sa_vec)
    
    return count_vectorizer_fn

def create_ngram_scorer(tokenizer_fn, ngrams, stopwords, weights=[]):
    
    if len(weights) != ngrams:
        raise Exception('weights must be an array of values equal in length to the number of ngrams')
    
    scorers = []
    for k in range(1, ngrams+1):
        count_vec = create_count_vectorizer_fn(tokenizer_fn, ngram_range=(k, k), stopwords=stopwords)
        scorers.append(count_vec)
        
    def ngram_scorer(ma, sa):
        scores = []
        for score_fn in scorers:
            score = score_fn(ma, sa)
            scores.append(score)
            
        scores = np.array(scores)
        return np.min([1.0, scores.dot(weights)])
    
    return ngram_scorer

# ===============================
# VECTOR SPACE MODEL FUNCTIONS
# ===============================

MAX_WMD_VALUE = 4.5

def create_wmsimilarity(model):

    def word_mover_similarity(s1, s2):
        """
        Uses gensims built in Word Mover Distance measurement.
        """
        value = model.wmdistance(s1, s2)
        if np.isinf(value) or np.isnan(value):
            return 0.0
        else:
            return max(1.0 - (value/MAX_WMD_VALUE), 0.0)
        
    return word_mover_similarity

def spacy_cosine_similarity(ma, sa):
    return ma.similarity(sa)


def create_word_to_word_similarity_function(model):

    def word_to_word_vector_similarity(v1, v2):
        n = len(v1)
        if n == 0:
            return 0.0
        
        sims = 0
        for i in range(len(v1)):
            x1 = v1[i]
            x2 = v2[i]

            try:
                sims += model.similarity(x1, x2)
            except KeyError:
                # word x1 or x2 is not in vocabulary
                pass

        return sims / n

    return word_to_word_vector_similarity

# ===============================
# SYNTAX FUNCTIONS
# ===============================

def get_node_label(node):
    if node.pos_ == 'PRON':
        return node.text.lower()
    else:
        # check for negation
        if 'neg' in [t.dep_ for t in node.children]:
            return 'neg_{}'.format(node.lemma_)
        else:
            return node.lemma_
        
def to_tree(node):
    if node.n_lefts + node.n_rights > 0:
        label = get_node_label(node)
        zss_Node = Node(label)
        for child in node.children:
            zss_Node.addkid(to_tree(child))
        return zss_Node
    else:
        label = get_node_label(node)
        zss_Node = Node(label)
        return zss_Node


def get_root(sent):
    return [t for t in sent if t.dep_ == 'ROOT'][0]


def tree_distance(s1, s2):
    tree1 = to_tree(get_root(s1))
    tree2 = to_tree(get_root(s2))
    return simple_distance(tree1, tree2)

def create_token_edit_distance(ignore=['DET', 'PUNCT'], stopwords=[]):
    
    def feature_token_edit_distance(ma, sa):
        ma1 = [get_node_label(t) for t in ma if len(t.dep_) > 0 and not t.pos_ in ignore]
        ma1 = [t for t in ma1 if not t in stopwords ]
        sa1 = [get_node_label(t) for t in sa if len(t.dep_) > 0 and not t.pos_ in ignore]
        sa1 = [t for t in sa1 if not t in stopwords]
        return edit_distance(sa1, ma1)
    
    return feature_token_edit_distance

def edit_distance(s1, s2):
    return nltk.edit_distance(s1,s2)


def edit_similarity(s1, s2):
    return (1 / (1 + edit_distance(s1, s2)))

def length_diff(ma, sa):
    return (len(ma) - len(sa)) / len(ma)


# ===============================
# HELPER FUNCTIONS
# ===============================

def create_feature(df, pipeline_fn, feature_fn, ma_column='MA'):
    
    values = []
    counter = 0
    
    for index, row in df.iterrows():
        
        ma = pipeline_fn( row[ma_column] )
        sa = pipeline_fn( row['SA'] )
        
        value = feature_fn(ma, sa)
        values.append(value)
        
        counter += 1
        if counter % 1000 == 0:
            print('[{}]: done'.format(counter))
        
        
    return np.array(values)


def create_feature_raw(df, feature_fn, ma_column='MA'):
    
    values = []
    counter = 0
    
    for index, row in df.iterrows():
        
        ma = row[ma_column]
        sa = row['SA']
        
        value = feature_fn(ma, sa)
        values.append(value)
        
        counter += 1
        if counter % 1000 == 0:
            print('[{}]: done'.format(counter))
        
        
    return np.array(values)


def run_features(df, feature_map):
    
    features = []
    columns = list(feature_map.keys())
    counter = 0
    
    for index, row in df.iterrows():
        
        feature_row = []
        
        for feature_key, feature_config in feature_map.items():
            ma_column = feature_config['column']
            ma = row[ma_column]
            sa = row['SA']
        
            feature_fn = feature_config['fn']
            
            if 'pipeline_fn' in feature_config.keys():
                pipeline_fn = feature_config['pipeline_fn']
                ma = pipeline_fn(ma)
                sa = pipeline_fn(sa)
                
            value = feature_fn(ma, sa)
            feature_row.append(value)
        
        features.append(feature_row)
        
        counter += 1
        if counter % 1000 == 0:
            print('[{}]: done'.format(counter))
    
    return pd.DataFrame(features, columns=columns)