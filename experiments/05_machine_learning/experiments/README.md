## Information for the Experiments

The experiments with the final results used in the paper are `e2_xview2` and `e2_xview2_labels`.

Unfortunately I did not have enough time to go through and visually clean up these experiments and their naming conventions.