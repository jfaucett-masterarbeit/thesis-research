import spacy
import nltk
from zss import simple_distance, Node
from nltk.corpus import stopwords
from nltk.translate.bleu_score import sentence_bleu, SmoothingFunction
import pandas as pd

SF = SmoothingFunction()


def get_node_label(node):
    if node.pos_ == 'PRON':
        return node.text.lower()
    else:
        # check for negation
        if 'neg' in [t.dep_ for t in node.children]:
            return 'neg_{}'.format(node.lemma_)
        else:
            return node.lemma_


def to_tree(node):
    if node.n_lefts + node.n_rights > 0:
        label = get_node_label(node)
        zss_Node = Node(label)
        for child in node.children:
            zss_Node.addkid(to_tree(child))
        return zss_Node
    else:
        label = get_node_label(node)
        zss_Node = Node(label)
        return zss_Node


def get_root(sent):
    return [t for t in sent if t.dep_ == 'ROOT'][0]


def tree_distance(s1, s2):
    tree1 = to_tree(get_root(s1))
    tree2 = to_tree(get_root(s2))
    return simple_distance(tree1, tree2)

def feature_tree_edit_distance(ma, sa, row):
    return tree_distance(ma, sa)


def dep_parse(sent, ignore=['det', 'punct']):
    return [t.dep_ for t in sent if not t.dep_ in ignore]

def create_feature_dp_edit_distance(ignore=['det', 'punct']):

    def feature_dp_edit_distance(ma, sa, row):
        ma1 = dep_parse(ma, ignore)
        sa1 = dep_parse(sa, ignore)
        return edit_distance(ma1, sa1)
    
    return feature_dp_edit_distance
    

def extract_pos_tags(sent, ignore=['DET', 'PUNCT']):
    return [t.pos_ for t in sent if not t.pos_ in ignore]

def create_feature_pos_edit_distance(ignore=['DET', 'PUNCT']):
    
    def feature_pos_edit_distance(ma, sa, row):
        ma1 = extract_pos_tags(ma, ignore)
        sa1 = extract_pos_tags(sa, ignore)
        return edit_distance(ma1, sa1)
    
    return feature_pos_edit_distance

def create_feature_token_edit_distance(ignore=['DET', 'PUNCT'], stopwords=[]):
    
    def feature_token_edit_distance(ma, sa, row):
        ma1 = [get_node_label(t) for t in ma if len(t.dep_) > 0 and not t.pos_ in ignore]
        ma1 = [t for t in ma1 if not t in stopwords ]
        sa1 = [get_node_label(t) for t in sa if len(t.dep_) > 0 and not t.pos_ in ignore]
        sa1 = [t for t in sa1 if not t in stopwords]
        return edit_distance(sa1, ma1)
    
    return feature_token_edit_distance


def edit_distance(s1, s2):
    return nltk.edit_distance(s1,s2)

def bleu_score(ma, sa, smoothing_function):
    return sentence_bleu([ma], sa, weights=[1, 0.3, 0.1], smoothing_function=smoothing_function)

def create_feature_bleu_score(smoothing_function=SF.method1, stopwords=[]):
    
    def feature_bleu_score(ma, sa, row):
        ma_t = [get_node_label(t) for t in ma if not t.lemma_ in stopwords and len(t.dep_) > 0]
        sa_t = [get_node_label(t) for t in sa if not t.lemma_ in stopwords and len(t.dep_) > 0]
        return bleu_score(ma_t, sa_t, smoothing_function)
    
    return feature_bleu_score


def execute_model(df, nlp, feature_map):
    
    columns = list(feature_map.keys())
    results = []
    counter = 0
    
    for index, row in df.iterrows():
        
        tmp = []
        for key, item in feature_map.items():
            score = item(nlp(row['MA']), nlp(row['SA']), row)
            tmp.append(score)
        
        results.append(tmp)
        
        counter += 1
        if counter % 100 == 0:
            print('[{}] completed'.format(counter))
        
    res = pd.DataFrame(results, columns=columns)
    return res