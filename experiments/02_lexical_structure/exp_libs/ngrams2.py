
# def ngram_sim(s1, s2, ngram_size):
#     index = ngram_size[0]
#     sims = []
#     while index <= ngram_size[1]:
#         ng = NGram(index)
#         sim = np.power(1 - ng.distance(s1, s2), index+1)
#         sims.append(sim)
#         index += 1
#     return np.mean(sims)


# def collect_evidence(s1, s2, ngram_size=(1,3)):
#     index = ngram_size[0]
#     out = []
#     while index <= ngram_size[1]:
#         ng = NGram(index)
#         dist = ng.distance(s1, s2)
#         out.append(dist)
#         index += 1
#     return np.array(out)

# def score_hits(distances):
#     index = 1
#     n = len(distances)
#     value = 0
#     for dist in distances:
#         value += np.power(dist, 1/(index + 1))

#     return value / (n + 1e-100)


# def evaluate_model(df, pipeline_fn, ma_size=1, ngram_size=(1, 3), which='best'):

#     y_true_raw = df['Score']
#     y_true = np.round(y_true_raw)
#     y_pred = []
#     y_pred_raw = []

#     for index, row in df.iterrows():
#         ma_res = []
#         sa_res = []

#         ma = pipeline_fn(row['MA'])
#         sa = pipeline_fn(row['SA'])
#         ma_score = ngram_sim(ma, sa, ngram_size)
#         ma_res.append(ma_score)

#         if ma_size > 1:
#             ma_size -= 1
#             ma2 = pipeline_fn(row['MA2'])
#             ma2_score = ngram_sim(ma, sa, ngram_size)
#             ma_res.append(ma2_score)

#         if ma_size > 1:
#             ma_size -= 1
#             ma3 = pipeline_fn(row['MA3'])
#             ma3_score = ngram_sim(ma, sa, ngram_size)
#             ma_res.append(ma3_score)

#         # take the best performing N-Gram match
#         if which == 'best':
#             idx = np.argmax(ma_res)
#             best_score = ma_res[idx]
#         elif which == 'mean':
#             best_score = np.mean(ma_res)
#         else:
#             raise Exception('unknown value for which: {}'.format(which))

#         score = np.round(5 * best_score)
#         y_pred_raw.append(best_score)
#         y_pred.append(score)

#     return {
#         'y_true_raw': y_true_raw,
#         'y_true': y_true,
#         'y_pred_raw': np.array(y_pred_raw),
#         'y_pred': np.array(y_pred)
#     }
