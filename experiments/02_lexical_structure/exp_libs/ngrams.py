import nltk
from similarity.ngram import NGram  # pip install strsim
from scipy.stats.mstats import gmean
import numpy as np
from sklearn.feature_extraction.text import CountVectorizer
from scipy import spatial

def create_count_vectorizer_fn(tokenizer_fn, ngram_range, stopwords):
    
    count_vec = CountVectorizer(tokenizer=tokenizer_fn, ngram_range=ngram_range, stop_words=stopwords)
    max_ng = ngram_range[1]
    
    def count_vectorizer_fn(ma, sa):
        if len(tokenizer_fn(ma)) < max_ng and len(tokenizer_fn(sa)) < max_ng:
            # not enough tokens to create the ngrams for any comparison
            return 0.0
        
        try:
            x = count_vec.fit_transform([ma, sa])
        except ValueError:
            return 0.0
        mat = x.toarray()
        ma_vec = np.array(mat[0])
        sa_vec = np.array(mat[1])
        value = spatial.distance.cosine(ma_vec, sa_vec)
        if np.isnan(value):
            return 0.0
        else:
            return 1 - value
    
    return count_vectorizer_fn

def create_ngram_scorer(tokenizer_fn, ngrams, stopwords, weights=[]):
    
    if len(weights) != ngrams:
        raise Exception('weights must be an array of values equal in length to the number of ngrams')
    
    scorers = []
    for k in range(1, ngrams+1):
        count_vec = create_count_vectorizer_fn(tokenizer_fn, ngram_range=(k, k), stopwords=stopwords)
        scorers.append(count_vec)
        
    def ngram_scorer(ma, sa):
        scores = []
        for score_fn in scorers:
            score = score_fn(ma, sa)
            scores.append(score)
            
        scores = np.array(scores)
        return np.min([1.0, scores.dot(weights)])
    
    return ngram_scorer
        

def create_ngrams(tokens, ngram_size=(1, 3)):
    index = ngram_size[0]
    out = {}
    while index <= ngram_size[1]:
        ng = list(nltk.ngrams(tokens, index))
        out['n{}'.format(index)] = [ng, index]
        index += 1
    return out


def collect_evidence(s1, s2, ngram_size=(1, 3)):

    s1_ngrams = create_ngrams(s1, ngram_size)
    s2_ngrams = create_ngrams(s2, ngram_size)

    hits = {}
    all_matches = {}

    for key in s1_ngrams.keys():
        s1_ng = s1_ngrams[key][0]
        s2_ng = s2_ngrams[key][0]

        hits[key] = {'hits': 0,
                     'size': len(s1_ng),
                     'ngram_size': s1_ngrams[key][1]}

        for g in s1_ng:
            for g2 in s2_ng:
                if g == g2:
                    hits[key]['hits'] += 1
                    if not g in all_matches:
                        all_matches[g] = 1

    return (hits, all_matches)


def score_hits(hits):
    # n = len(hits.keys())

    # Setup Weights
    w = []
    for k in hits.keys():
        size = hits[k]['ngram_size']
        w.append(size)

    weights = 1 / np.array(w)
    # w = w / (np.max(w) + (1e-100))
    # w = np.sum(w / (np.max(w) + (1e-100)))

    # Calculate scores for each ngram section
    x = []
    for key in hits.keys():
        #power = hits[key]['ngram_size'] + 2
        hit_counts = hits[key]['hits']
        size = hits[key]['size']
        if size > 0:
            tmp = hit_counts / size
            x.append(tmp)
        else:
            x.append(0.0)

    x = np.array(x)
    score = np.sum(np.power(x, 1/weights))
    #print(x, w, weights, score)
    #score = np.sum(x) / (np.sum(w) + (1e-100))
    return np.min([1.0, score])


def evaluate_model(df, pipeline_fn, ma_size=1, ngram_size=(1, 3), store_ngrams=False):

    y_true_raw = df['Score']
    y_true = np.round(y_true_raw)
    y_pred = []
    y_pred_raw = []
    ngram_data = []

    for _index, row in df.iterrows():
        tmp_ma_size = ma_size
        ma_res = []
        sa_res = []

        ma = pipeline_fn(row['MA'])
        sa = pipeline_fn(row['SA'])

        # model answer hits
        ma_hits, _ma_matches = collect_evidence(ma, sa, ngram_size)
        ma_hit_score = score_hits(ma_hits)
        ma_res.append(ma_hit_score)

        # student answer hits
        sa_hits, _sa_matches = collect_evidence(sa, ma, ngram_size)
        sa_hit_score = score_hits(sa_hits)
        sa_res.append(sa_hit_score)
        
        if store_ngrams:
            ngram_tmp = { 'ma' : [], 'sa' : [] }
            ngram_tmp['ma'].append(_ma_matches)
            ngram_tmp['sa'].append(_sa_matches)

        if tmp_ma_size > 1:
            tmp_ma_size -= 1
            ma2 = pipeline_fn(row['MA2'])
            ma2_hits, _ma2_matches = collect_evidence(ma2, sa, ngram_size)
            ma2_hit_score = score_hits(ma2_hits)
            ma_res.append(ma2_hit_score)

            sa2_hits, _sa2_matches = collect_evidence(sa, ma2, ngram_size)
            sa2_hit_score = score_hits(sa2_hits)
            sa_res.append(sa2_hit_score)
            
            if store_ngrams:
                ngram_tmp['ma'].append(_ma2_matches)
                ngram_tmp['sa'].append(_sa2_matches)

        if tmp_ma_size > 1:
            tmp_ma_size -= 1
            ma3 = pipeline_fn(row['MA3'])
            ma3_hits, _ma3_matches = collect_evidence(ma3, sa, ngram_size)
            ma3_hit_score = score_hits(ma3_hits)
            ma_res.append(ma3_hit_score)

            sa3_hits, _sa3_matches = collect_evidence(sa, ma3, ngram_size)
            sa3_hit_score = score_hits(sa3_hits)
            sa_res.append(sa3_hit_score)
            
            if store_ngrams:
                ngram_tmp['ma'].append(_ma3_matches)
                ngram_tmp['sa'].append(_sa3_matches)

        idx = np.argmax(ma_res)
        ma_hit_score_mx = ma_res[idx]
        sa_hit_score_mx = sa_res[idx]

        # take geometric mean of both scores: this penalizes long student answers
        # which contain the model answer somewhere within them.
        hit_score = gmean([ma_hit_score_mx, sa_hit_score_mx])

        score = np.round(5 * hit_score)
        y_pred_raw.append(hit_score)
        y_pred.append(score)
        
        if store_ngrams:
            ngram_data.append(ngram_tmp)

    return {
        'y_true_raw': y_true_raw,
        'y_true': y_true,
        'y_pred_raw': np.array(y_pred_raw),
        'y_pred': np.array(y_pred),
        'ngrams' : np.array(ngram_data)
    }

def evaluate_func(df, bow_fn, ma_size=1):

    y_true_raw = df['Score']
    y_true = np.round(y_true_raw)
    y_pred = []
    y_pred_raw = []

    for _index, row in df.iterrows():
        tmp_ma_size = ma_size
        ma_res = []
        sa_res = []

        ma = row['MA']
        sa = row['SA']

        # model answer hits
        ma_hit_score = bow_fn(ma, sa)
        ma_res.append(ma_hit_score)

        if tmp_ma_size > 1:
            tmp_ma_size -= 1
            ma2 = row['MA2']
            ma2_hit_score = bow_fn(ma2, sa)
            ma_res.append(ma2_hit_score)

        if tmp_ma_size > 1:
            tmp_ma_size -= 1
            ma3 = row['MA3']
            ma3_hit_score = bow_fn(ma3, sa)
            ma_res.append(ma3_hit_score)

        idx = np.argmax(ma_res)
        hit_score = ma_res[idx]

        score = np.round(5 * hit_score)
        y_pred_raw.append(hit_score)
        y_pred.append(score)

    return {
        'y_true_raw': y_true_raw,
        'y_true': y_true,
        'y_pred_raw': np.array(y_pred_raw),
        'y_pred': np.array(y_pred)
    }

def inspect_results(df, y_true, y_pred, idxs, match_data, size=10):
    y_true = np.array(y_true)
    y_pred = np.array(y_pred)
    
    tmp = df[idxs]
    
    idxsr = np.random.choice(len(tmp), size=size)
    t_scores = y_true[idxs][idxsr]
    p_scores = y_pred[idxs][idxsr]
    md_entries = match_data[idxs][idxsr]
    
    t = tmp.iloc[idxsr]
    
    idx = 0
    for index, row in t.iterrows():
        
        print('=======================')
        print('Prompt: {}'.format(row['Prompt']))
        print('Score: {}, Score-Pred: {}'.format(t_scores[idx], p_scores[idx]))
        print('MA: {}'.format(row['MA']))
        print('MA_ngrams: {}'.format(md_entries[idx]['ma']))
        print('SA: {}'.format(row['SA']))
        print('SA_ngrams: {}'.format(md_entries[idx]['sa']))
        print('=======================')
        idx += 1