import numpy as np

def convert_to_pass_fail(scores, thresh=2.5):
    """
    Converts a list of scores to a binary classification.
    Every value < thresh is assigned the value of 0.
    Every value >= thresh is assigned the value of 1.

    Example:

        >>> x = np.array([1,2,3,4,5])
        >>> res = convert_to_pass_fail(x, thresh=2.5)
        >>> print(res)
        [0,0,1,1,1]

    """
    tmp = scores.copy()
    tmp[tmp < thresh] = 0
    tmp[tmp >= thresh] = 1
    return tmp

def convert_to_class3(scores, thresh=[2.5, 4]):
    
    tmp = scores.copy()
    tmp[tmp < thresh[0]] = 0
    tmp[np.logical_and(tmp >= thresh[0], tmp < thresh[1])] = 1
    tmp[tmp >= thresh[1]] = 2
    
    return tmp

def min_max_normalization(x):
    mn = np.min(x)
    mx = np.max(x)
    return (x - mn) / (mx - mn)