import pandas as pd
import numpy
import os
import pickle

PROJECT_ROOT = os.path.dirname(os.path.dirname(__file__))

DATASETS_DIR = os.path.join(PROJECT_ROOT, "datasets")


def save(filepath, data):
    """
    Saves `data` (any Python object) as a serialized data structure
    to a file at `filepath`.
    """
    with open(filepath, 'wb') as f:
        pickle.dump(data, f)


def load(filepath):
    """
    Loads any saved Python object from its serialized form stored at `filepath`
    into the current Python context.
    """
    with open(filepath, 'rb') as f:
        data = pickle.load(f)
    return data


def load_dataframe(filepath):
    """
    Loads a CSV file specified by `filepath` as a pandas DataFrame.
    It assumes the file has a seperator=`~` and no index column.
    """
    return pd.read_csv(filepath, sep='~', index_col=False)


def load_dataset(language='en', version=1, suffix=None):
    """
    Loads a research dataset as a pandas DataFrame given a `language` and optionally a `version`
    and file `suffix`.
    It only loads CSV files from the datasets/base directory.
    """
    if suffix:
        filename = 'asag_v{}_{}_{}.csv'.format(version, language, suffix)
    else:
        filename = 'asag_v{}_{}.csv'.format(version, language)
    filepath = os.path.join(DATASETS_DIR, "base", filename)
    df = pd.read_csv(filepath, sep='~', index_col=False)
    return df


def merge_dataframes(*dataframes):
    """
    A Helper method which merges multiple pandas DataFrames into
    one DataFrame.
    """
    return pd.concat(dataframes, axis=1)
