from nltk.stem.snowball import SnowballStemmer
from nltk.corpus import stopwords
from nltk.tokenize import WhitespaceTokenizer
import string

# STOPWORDS
EN_STOPS = stopwords.words('english')
DE_STOPS = stopwords.words('german')
ES_STOPS = stopwords.words('spanish')

# STEMMERS
EN_STEMMER = SnowballStemmer('english')
DE_STEMMER = SnowballStemmer('german')
ES_STEMMER = SnowballStemmer('spanish')

WHITESPACE_TOKENIZER = WhitespaceTokenizer()


def create_pipeline(pipeline_map, tokenizer):

    def pipeline(sentence):
        tokens = tokenizer(sentence)
        result = tokens.copy()
        for step, func in pipeline_map.items():
            try:
                result = func(result)
            except Exception as err:
                print("[{}]: failed.".format(step))
                print("[reason]: {}".format(err))

        return result

    return pipeline


def clean_word(word):
    word = word.strip()
    word = word.replace(',', '').replace('.', '').replace('_', '')
    return word

def whitespace_tokenize(sentence):
    return WHITESPACE_TOKENIZER.tokenize(sentence)

def stem_sentence(sentence, stemmer=EN_STEMMER):
    return [stemmer.stem(word) for word in sentence]

def create_remove_stopwords(stopwords):
    
    def remove_stopwords(sentence):
        return [word for word in sentence if not word in stopwords]
    
    return remove_stopwords

def case_normalization(sentence):
    return [word.lower() for word in sentence]

def punctuation_removal(sentence):
    return [clean_word(word) for word in sentence if len(clean_word(word)) > 0]

def create_stemmer(stemmer):
    
    def stem_sent(sentence):
        return [stemmer.stem(word) for word in sentence]

    return stem_sent