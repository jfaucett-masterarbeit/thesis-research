from uuid import uuid4
import numpy as np
import pandas as pd


class ExperimentRunner:

    def __init__(self, df, components=[]):
        self.components = components
        self.df = df
        self.run_id = ''
        self.state = {}

    def run(self):
        self.run_id = str(uuid4())

        results = []
        metas = []

        run_state = self.state.copy()

        for _index, row in self.df.iterrows():

            f_results = []
            f_metas = []

            for component in self.components:
                f_result, f_meta = component.execute(self.df, run_state, row)
                f_results.append(f_result)
                f_metas.append(f_meta)

            results.append(f_results)
            metas.append(f_metas)

        feature_df = pd.DataFrame(
            results, columns=[x.name for x in self.components])

        self.results = {
            'features': feature_df,
            'meta': metas
        }


class Component:

    def __init__(self, name, handler):
        self.name = name
        self.handler = handler

    def execute(self, df, state, row):
        raise Exception('implement')
