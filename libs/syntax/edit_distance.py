import nltk
import numpy as np


def edit_similarity(source_sequence, target_sequence):
    mx_len = np.max([len(source_sequence), len(target_sequence)])

    if mx_len > 0:
        return 1.0 - (edit_distance(source_sequence, target_sequence) / mx_len)
    else:
        return 0.0


def edit_distance(source_sequence, target_sequence):
    return nltk.edit_distance(source_sequence, target_sequence)
