import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import pandas as pd

from cycler import cycler

# Styles
monochrome = (cycler('color', ['k']) * cycler('linestyle',
                                              ['-', '--', ':', '=.']) * cycler('marker', ['^', ',', '.']))
bar_cycle = (cycler('hatch', ['///', '--', '...', '\///', 'xxx',
                              '\\\\']) * cycler('color', 'w')*cycler('zorder', [10]))
styles = bar_cycle()


def render_accuracy_scores(df, title, ylim, cols=[], xticks=[], axis_labels={}, number_info={'fontsize':18, 'xoffset': -0.1, 'yoffset' : 0.8}, savefile='', best_line=-1, worst_line=-1):

    fig, ax = plt.subplots(1, 1)
    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20)
    ax.set_prop_cycle(monochrome)

    x = np.arange(len(df))
    
    # Grid Setup
    ax.grid(linestyle='--')
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.spines['bottom'].set_visible(False)
    ax.spines['left'].set_visible(False)

    # Labels
    ax.set_title(title['title'], size=title['fontsize'])
    if len(axis_labels) > 0:
        ax.set(xlabel=axis_labels['xlabel'])
        ax.xaxis.label.set_fontsize( axis_labels['fontsize'])
        # ax.xaxis.padlabel = 40
        ax.set(ylabel=axis_labels['ylabel'])
        ax.yaxis.label.set_fontsize( axis_labels['fontsize'])
    
    labels = cols.copy()
    hatches = ['//', 'xxx', '\\', '///', 'xx']

    # Render
    plt.ylim([0,ylim])
    offset = 0
    width = 0.2
    idx = 0
    pos = []
    for column in cols:
        hatch = hatches[idx]
        value = df[column] * 100
        ax.bar(x+offset, width=width, height=value, hatch=hatch, fill=False)
        
        pos.append(x+offset)
        offset += width
        idx += 1
        
    plt.legend('best', labels=labels)
    plt.xticks(x + width, xticks)
    
    # Add numeric values
    pos = np.array(pos)
    x_pos = pd.DataFrame(pos.T, columns=cols)
    
    for i in range(0, len(df)):
        row = df.iloc[i]
        for col in cols:
            value = row[col]*100
            y_off = number_info['yoffset']
            x_off = number_info['xoffset']
            
            plt.text(x=x_pos[col].iloc[i]+x_off, y=value+y_off, s='{:.1f}'.format(value), size=number_info['fontsize'])
            
    # horizontal lines
    if best_line >= 0:
        ax.axhline(best_line, color='#80e16d')
    if worst_line >= 0:
        ax.axhline(worst_line, color='#e54e4e')
        
    
    plt.tight_layout()
    if len(savefile) > 0:
        plt.savefig('{}'.format(savefile))
    plt.show()