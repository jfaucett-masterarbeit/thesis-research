import numpy as np
from sklearn.metrics import classification_report, cohen_kappa_score, accuracy_score, precision_score, recall_score, f1_score
from sklearn.metrics import confusion_matrix
import matplotlib.pyplot as plt
import itertools

def covariance(x, y):
    mx = np.mean(x)
    my = np.mean(y)

    return (1/len(x)) * np.sum((x-mx) * (y-my))


def pearsons_r(y_true, y_pred):
    r = covariance(y_true, y_pred) / (np.std(y_true) * np.std(y_pred))
    return r


def mean_absolute_error(y_true, y_pred):
    return np.mean(np.array(y_true) - np.array(y_pred))


def print_big4(y_true, y_pred):
    print('Accuracy: {:.2f}%'.format(accuracy_score(y_true, y_pred) * 100))
    print('Precision: {:.2f}%'.format(precision_score(
        y_true, y_pred, average='weighted') * 100))
    print('Recall: {:.2f}%'.format(recall_score(
        y_true, y_pred, average='weighted') * 100))
    print('F1-Measure: {:.2f}%'.format(f1_score(y_true,
                                                y_pred, average='weighted') * 100))


def print_all_stats(y_true, y_pred):
    print(classification_report(y_true, y_pred))
    print("Pearson's R: {}".format(pearsons_r(y_true, y_pred)))
    print("Cohen's Kappa: {}".format(cohen_kappa_score(y_true, y_pred)))
    print("MAE: {}".format(mean_absolute_error(y_true, y_pred)))
    print_big4(y_true, y_pred)
    
    
def get_all_stats(y_true, y_pred):
    return {
        'apcf' : [
            accuracy_score(y_true, y_pred), 
            precision_score(y_true, y_pred, average='weighted'),
            recall_score(y_true, y_pred, average='weighted'),
            f1_score(y_true,y_pred, average='weighted')
        ],
        'r' : pearsons_r(y_true, y_pred),
        'k' : cohen_kappa_score(y_true, y_pred),
        'mae' : mean_absolute_error(y_true, y_pred)
    }

def cm(y_true, y_pred):
    return confusion_matrix(y_true, y_pred)


def plot_confusion_matrix(cm, classes, normalize=False, title='Confusion matrix', cmap='gray', savefile='',xlabel='Predicted', ylabel='True'):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]

    thresh = cm.max() / 2.

    plt.imshow(cm, interpolation='nearest', cmap=plt.cm.Greys)
    plt.title(title, size=18)
    # plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45, fontsize=14)
    plt.yticks(tick_marks, classes, fontsize=14)

    fmt = '.2f' if normalize else 'd'

    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, format(cm[i, j], fmt),
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black", size=14)

    plt.tight_layout()
    plt.ylabel(ylabel, size=16)
    plt.xlabel(xlabel, size=16)
    if len(savefile) > 0:
        plt.savefig('{}_cm_bw.png'.format(savefile), bbox_inches = "tight")
    plt.show()