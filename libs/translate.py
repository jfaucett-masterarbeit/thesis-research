# filename: translate.py
# author: John Faucett
# summary:
#   This file provides functions and classes for translating a data from a source language to a target language.
#

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from googletrans import Translator
import time


def translate(t, text, dest='de', src='en'):
    return t.translate(text, src=src, dest=dest).text


def random_wait(n=1):
    timespan = np.random.random() * n
    time.sleep(timespan)


class TranslateSeries:

    def __init__(self, input_series, source='en', target='de', mean_wait=4, log=False):
        self.input_series = input_series
        self.source = source
        self.target = target
        self.wait = mean_wait*2
        self.translator = Translator()
        self.log = log

    def translate(self):

        curr_id = self.input_series[0]
        prev_id = '--none--'
        out = []
        result = ''
        counter = 0

        for _idx, value in self.input_series.iteritems():
            curr_id = value

            if curr_id != prev_id:
                # translate the entry and randomly wait
                result = translate(self.translator, value,
                                   src=self.source, dest=self.target)
                random_wait(self.wait)

                if self.log:
                    print('New Result: {}'.format(result))
                prev_id = curr_id

            out.append(result)
            counter += 1

            if counter % 300 == 0:
                print('Entries: {}'.format(counter))

        return out


class TranslateVector:

    def __init__(self, input_vec, source='en', target='de', mean_wait=4, log=False):
        self.input_vec = input_vec
        self.source = source
        self.target = target
        self.wait = mean_wait*2
        self.translator = Translator()
        self.log = log

    def translate(self):
        out = []
        for raw_text in self.input_vec:

            # translate the entry and randomly wait
            result = translate(self.translator, raw_text,
                               src=self.source, dest=self.target)
            random_wait(self.wait)
            if self.log:
                print('Result: {}'.format(result))

            out.append(result)
        return out
