## Research Paper

#### Folder Organization

1. **datasets/**: stores all the parsed and formatted datasets that were used for the research.
   - **annotations.md**: contains notes, insights and thoughts jotted down while hand grading student responses.
   - **short_answer_types.md**: failed foray into trying to assign student responses to categories.
2. **experiments/**: holds notebooks of research experiments for each one of the main experiment categories.
   - **00_dataset_exploration**: various notebooks which demonstrate my exploration of the datasets.
   - **01_regular_expressions**: notebooks for the pattern matching experiment. Also contains the data sent to test participants.
   - **02_lexical_structure**: notebooks for lexical structure experiments (word-overlap, n-grams, bag-of-words)
   - **03_syntax_structure**: experiments in looking at syntax correlations
   - **04_semantics**: experiments looking at knowledge based approaches and word embeddings.
   - **05_machine_learning**: experiments looking at applying machine learning models to features based on knowledge gained in previous experiments.
3. **libs**: stores library functions and classes that are used throughout the project. Some of these files are used in the experiments, some in data exploration scripts, etc.
4. **notes**: todos and random notes
5. **presentation**: some statistics and renderings completed for the presentation of the results.

#### Experiment Folder Structure

All of the major experiment categories (starting with the regular expression category) have roughly the same folder structure.

1. `experiments`: contains all the jupyter notebooks for each experiment run.
2. `exp_libs`: contains functions and classes programmed to run the particular experiments.
3. `renderings`: folder acts as a storage place for graphics and plots.
4. `tmp`: stores data and files such as statistics from an experiment. So that they can be easily accessed and evaluated again without having to run the whole experiment.

## Setup

#### Installing Libraries

Be sure you have Python installed. The research was written and tested for Python 3.6.4 but should work so long as you are using a Python version greater than 3.0.

I recommend installing a current version of [Anaconda](https://www.anaconda.com/download/), since it comes with [Jupyter Notebooks](https://jupyter.org/) which are also needed.

To install the required python libraries needed for all the experiments, run the following in the terminal.

```bash
pip install -r scripts/requirements.txt
```

#### Downloading NLP Data

##### NLTK

Open up a terminal and issue the following commands and be sure to download all the NTLK datasets.

```python
import nltk
nltk.download()
```

##### SpaCy

SpaCy word embeddings are also used for some of the experiments. To just run the english experiments run:

```bash
python -m spacy download en
```

If you want to also be able to run Spanish and German language experiments, you'll need to download those word embeddings as well.

```bash
python -m spacy download es
python -m spacy download de
```

##### Experiment Data

The word embedding experiments use different pre-trained word embedding models. These are all too large to include in git and track. In order to be
able to run the experiments, one has to download these models and possibly change the paths to the models in the experiments.

The following commands can be run from a Python console and download gensim word embedding models that are used in the experiments.

```python
import gensim.downloader as api
api.load('glove-wiki-gigaword-300')
api.load('fasttext-wiki-news-subwords-300')
```

To be able to run the experiments which test against German and Spanish word embedding models the following steps must be completed.

First, run the `setup_vector_models.sh` script.

```bash
chmod +x setup_vector_models.sh # if not already executable
./setup_vector_models.sh
```

This creates the necessary folder structure.

Now download the Spanish word embeddings in FastText and Word2Vec

1. [Spanish Word2Vec](https://drive.google.com/open?id=0B0ZXk88koS2KNGNrTE4tVXRUZFU)
2. [Spanish FastText](https://www.dropbox.com/s/irpirphmieg4klv/es.tar.gz?dl=0)

into the folders `vector_models/small_models/es/fast_text` and `vector_models/small_models/es/w2v` so that the fast text word embedding is in the `fast_text` dir and word2vec is in the `w2v` directory. Extract them inside those folders.

And German word embeddings

1. [German Word2Vec](https://drive.google.com/open?id=0B0ZXk88koS2KLVVLRWt0a3VmbDg)
2. [German FastText](https://www.dropbox.com/s/jy6taiacmptr537/de.tar.gz?dl=0)

Into the folders `vector_models/small_models/es/fast_text` and `vector_models/small_models/es/w2v` and extract them just as above.

Now these models can be loaded in the experiments. All of these word embedding models can be found at [Pretrained Word Embeddings on Github](https://github.com/Kyubyong/wordvectors) as well as other word embedding models in other languages.

#### Notes

1. [Link to old experiments](https://gitlab.com/jfaucett-masterarbeit/thesis-paper-research-old)
2. [Pretrained Word Embedding Models in 30+ Languages](https://github.com/Kyubyong/wordvectors)
