## Types of Short Answers

1. Item Listing (Any Order): 
  [1.2, 1.3, 2.1, 3.6, 3.7, 4.1]
2. Concrete Question (Compare/Contrast, Elaborate or Give Details, More Open Ended):
  [1.1, 1.6, 1.7, 2.2, 2.3, 2.4, 2.6, 4.4, 7.5, 8.3, 8.4, 8.5, 9.3,
   9.4, 11.7, 11.9, 12.5, 12.9, 11.1]
3. Give Definition (Knowledge or Simple Fact): 
  [1.5, 2.7, 3.1, 3.3, 4.5, 5.1, 5.2, 5.3, 5.4, 5.5, 6.1, 6.2,
   6.3, 6.4, 6.5, 6.6, 6.7, 7.1, 8.1, 8.6, 8.7, 9.1, 10.1, 10.2, 10.3,
   10.4, 10.5, 10.6, 10.7, 11.4, 11.5, 11.10, 12.1, 12.2, 12.4, 12.6, 12.8, 12.10]
4. Ambiguous Phrasing:
  [2.4]
5. Main Advantage / Key Attributes of X (Many Items choose top K):
  [3.4, 4.2, 7.2, 7.3, 7.6, 7.7, 9.5, 11.8]
6. Describe Process or Conditions necessary for X:
  [2.4, 3.5, 3.6]
7. Item Listing (Order Matters):
  [12.3]
