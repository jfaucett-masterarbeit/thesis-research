## Dataset Annotations

When extending the raw datasets the following categories were used to provide further details about student responses.

#### Grade Categories

1. wrong_unrelated: Response is wrong and not even related to the question prompt; or the student did not even answer
2. wrong_related: Reponse is wrong but still related the the domain of the question.
3. partially_correct: Response is partially correct but for various reasons does not fully satisfy the desired answer.
4. correct_complete: response gets every aspect question correctly.
5. correct_indirect: response gets everything correct but requires that the grader make inferences and interpretations to grant that correctness.

#### Grading Annotations/Labels

1. missed_concept: student failed to get one or more important concept(s)
2. partially_missed_concept : student missed part of a concept
3. extra_info: student provided (significant) extra information which is correct but not needed.
   - ex: 8.6 1479
4. shows_lack_of_knowledge: student text shows lack of knowledge about topic
5. contains_false_assertion: student text has a false assertion
6. question_misinterpretation: student misread the question
7. not_enough_info (to demonstrate understanding): student doesn't show understanding with his answer (though it may be correct)
   - ex: 8.6 1457, 1463
   - ex: 10.7 1844
8. misspelling: student misspelled a key concept/idea
   - ex: 11.5, 1984
   - ex: 11.10, 2158
9. confusing_wording: student response was difficult to understand or awkwardly worded such that it impedes comprehension.
10. other_wording: student got the idea/concept but wording was significantly different or possible requires inferential knowledge by reader. (Ex: 2.2.25)
11. no_response: student did not answer the question

#### Grading Notes

1. See 2.4: Often a model answer has 2 or more concepts and it is unclear whether the student should get all concepts or whether answering one of them counts for full credit.
2. Short Model Answers:
   - 1.3, 1.4, 2.5, 8.2, 9.2, 9.6, 9.7, 11.2, 11.3, 11.5, 11.6, 12.7
3. Model Answer Problems: 8.5
4. Example of Adding "Alternative Spellings" feature: 9.1.1513 (FIFO, first-in, first-out, First in First out)
5. Short Answer where ordering matters: 12.3

### Note 1:

In some contexts adding extra information leads to counting off points. Example: 12.7. Push and Pop vs. listing any method a student can think of, somewhere in the list is push and pop.

### Note 2:

Strategically it is better to be vague, less specific, but still correct over highly specific and incorrect, even though both answers may essentially contain the same information. (ex: 12.9)

### Note 3:

7.1 What is a linked list? shows an example where a student perfectly describes a linked list yet none of the words overlap or are even related to the description given in the model answer.
