## This document stores some insights made while exploring the datasets

### What are really difficult aspects?

1. Answers which contain some correct things and some incorrect aspects.
2. Answers that are completely unrelated.
3. Synonymy.
4. Answers that are somewhere in the middle of grading.
5. Word Order:
 - all humans are philosophers vs. all philosophers are human.
6. Negation:
 - there are unbalanced forces vs. there are no unabalanced forces.


### How to make good model answers?

1. Use real student answers
2. Allow for clearly distinguishing between correct/incorrect answers.

### Why are people not using this?

1. Can be precise but very frustrating (low recall scores)
2. If you improve recall you let a lot of garbage through
3. Need for some amount of teacher effort
4. The more graded student responses the better the ASAG system

### Good Application Domains

1. Many Students and questions should be reused.
2. Is the question appropriate for short-answer?
